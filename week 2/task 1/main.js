const nameInput = document.getElementById("name");
const bDateInput = document.getElementById("date");
const bTimeInput = document.getElementById("time");
const emailInput = document.getElementById("email");
const imgInput = document.getElementById("img");
const countryInput = document.getElementById("country");
const genderInput = document.getElementsByName("gender");
const hobbiesInput = document.getElementsByName("hobbies");
const error = document.getElementById("error");


let today = new Date();

let date = today.getDate();
if (date < 10) date = "0" + date;

console.log(date);

let month = (today.getMonth()) + 1
if (month < 10) month = "0" + month;

let year = today.getFullYear();

let todayDate = year + "-" + month + "-" + date;

// console.log(todayDate);
bDateInput.max = todayDate;


const exp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;




function validateForm(){
    if (nameInput.value == ""){
        error.innerHTML = "Please Enter Your Name";
    }else if(bDateInput.value == ""){
        error.innerHTML = "Please Enter Your Full Birth Date";
    }else if(bTimeInput.value == ""){
        error.innerHTML = "Please Enter Your Birth Time";
    }else if (emailInput.value == "") {
        error.innerHTML = "Please Enter Your Email";
    }else if (!(exp.test(emailInput.value))) {
        error.innerHTML = "Please Enter Your Valid Email";
    }else if (imgInput.value == "") {
        error.innerHTML = "Please Provide Photo Proof";
    }else if( !(genderInput[0].checked || genderInput[1].checked || genderInput[2].checked) ){
        error.innerHTML = "Please Select Gender";
    }else if( !(hobbiesInput[0].checked || hobbiesInput[1].checked || hobbiesInput[2].checked || hobbiesInput[2].checked) ){
        error.innerHTML = "Please Select Atleast One Hobby";
    }else if (countryInput.value == "0"){
        error.innerHTML = "Please Select Your Country";
    }else{
        error.innerHTML = "";
        alert("Your Information is valid");
    }
}