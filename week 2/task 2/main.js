$(function () {
    let first_name = $("#first_name");
    let last_name = $("#last_name");
    let email = $("#email");
    $("#newBtn").click(() => {
        $(".form").show();
        $("#newBtn").hide();
    });
    let tbody = $("#tableBody");
    // console.log(tbody)

    //GET method
    // $.ajax({
    //     type: 'GET',
    //     url: 'https://reqres.in/api/users?page=2',
    //     success: (data) => {
    //         // console.log(data)
    //         data.data.forEach(element => {
    //             tbody.append(`<tr id="tr${element.id}">
    //         <th scope="row">${element.id}</th>
    //         <td>${element.first_name}</td>
    //         <td>${element.last_name}</td>
    //         <td>${element.email}</td>
    //         <td><button id="u${element.id}" class="update"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-fill" viewBox="0 0 16 16">
    //         <path d="M12.854.146a.5.5 0 0 0-.707 0L10.5 1.793 14.207 5.5l1.647-1.646a.5.5 0 0 0 0-.708l-3-3zm.646 6.061L9.793 2.5 3.293 9H3.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.207l6.5-6.5zm-7.468 7.468A.5.5 0 0 1 6 13.5V13h-.5a.5.5 0 0 1-.5-.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.5-.5V10h-.5a.499.499 0 0 1-.175-.032l-.179.178a.5.5 0 0 0-.11.168l-2 5a.5.5 0 0 0 .65.65l5-2a.5.5 0 0 0 .168-.11l.178-.178z"/>
    //       </svg></button></td>
    //         <td><button class="remove" id="d${element.id}" ><b>X</button></td>
    //     </tr>`);
    //         });
    //     }
    // })

    //cancel button
    $("#cancelBtn").click(() => {
        $(".form")[0].reset();
        $(".form").hide();
        $("#newBtn").show();
    })

    //addbutton
    $("#addBtn").click((e) => {
        if (first_name.val() == "" || last_name.val() == "" || email.val() == "") {
            alert("Fill All The details");
        } else {
            // e.preventDefault()
            // console.log("u clicked");
            // console.log(first_name.val(),last_name.val(),email.val())
            let details = {
                "first_name": first_name.val(),
                "last_name": last_name.val(),
                "email": email.val(),
            };
            // console.log(details);
            //POST method
            $.ajax({
                type: 'POST',
                url: "https://reqres.in/api/users",
                data: details,
                success: (data) => {
                    tbody.append(`<tr id="tr${data.id}">
                                    <th scope="row">${data.id}</th>
                                    <td id="fn${data.id}">${data.first_name}</td>
                                    <td id="ln${data.id}">${data.last_name}</td>
                                    <td id="e${data.id}">${data.email}</td>
                                    <td>
                                        <button id="${data.id}" class="update"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-fill" viewBox="0 0 16 16">
                                        <path d="M12.854.146a.5.5 0 0 0-.707 0L10.5 1.793 14.207 5.5l1.647-1.646a.5.5 0 0 0 0-.708l-3-3zm.646 6.061L9.793 2.5 3.293 9H3.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.207l6.5-6.5zm-7.468 7.468A.5.5 0 0 1 6 13.5V13h-.5a.5.5 0 0 1-.5-.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.5-.5V10h-.5a.499.499 0 0 1-.175-.032l-.179.178a.5.5 0 0 0-.11.168l-2 5a.5.5 0 0 0 .65.65l5-2a.5.5 0 0 0 .168-.11l.178-.178z"/>
                                        </svg></button>
                                    </td>
                                    <td><button class="remove" data_id="${data.id}" ><b>X</button></td>
                                </tr>`);
                    $(".form")[0].reset();
                    $(".form").hide();
                    $("#newBtn").show();
                }
            })
        }
    })
    tbody.delegate('.remove', 'click', function () {
        let tr = $(this).closest("tr")
        //console.log(tr)
        $.ajax({
            type: 'DELETE',
            url: "https://reqres.in/api/users/" + $(this).attr("data_id"),
            success: (url) => {
                tr.remove();
            }
        })
    })
    
    tbody.delegate('.update', 'click', function () {
        $(".form").show();
        $("#addBtn").hide();
        $("#cancelBtn").hide();
        $("#updateBtn").show();
        let id = $(this).attr("id");
        let fn = "#fn"+id;
        let ln = "#ln"+id;
        let eid = "#e"+id;
        $("#updateBtn").click(function () {
            let details={}
            let fname = first_name.val()
            let lname = last_name.val()
            let uemail = email.val()
            if (fname!=""){
                details.first_name = fname;
            }
            if (lname!=""){
                details.last_name = lname;
            }
            if (uemail!=""){
                details.email = uemail;
            }

            $.ajax({
                type: 'PUT',
                url: 'https://reqres.in/api/users/' + id,
                data:details,
                success:function (data) {
                    if (fname!=""){
                        $(fn).html(fname);
                        // console.log($(fn))
                    }
                    if (lname!=""){
                        $(ln).html(lname);
                    }
                    if (uemail!=""){
                        $(eid).html(uemail);
                    }
                    $(".form")[0].reset();
                    $(".form").hide();
                    $("#newBtn").show();
                    //console.log("updated")
                    
                }
            })
        })
    })
})